<?php
/**
 * Astound Notifications Adminhtml abstract controller.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Controller\Adminhtml;

/**
 * Class Notifications
 * @package Astound\Notifications\Controller\Adminhtml
 */
abstract class Notifications extends \Magento\Backend\App\Action
{
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Astound_Notifications::notification_bars'
        );
    }
}
