<?php
/**
 * Astound Notifications bar edit form Delete action.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Controller\Adminhtml\Notifications;

use Astound\Notifications\Controller\Adminhtml\Notifications;
use Magento\Backend\App\Action\Context;
use Astound\Notifications\Model\Bars;

/**
 * Class Delete
 * @package Astound\Notifications\Controller\Adminhtml\Notifications
 */
class Delete extends Notifications
{
    /**
     * @var \Astound\Notifications\Model\Bars
     */
    protected $barsModel;

    /**
     * Delete constructor.
     *
     * @param Context $context
     * @param Bars $barsModel
     */
    public function __construct(
        Context $context,
        Bars $barsModel
    ) {
        parent::__construct($context);
        $this->barsModel = $barsModel;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');

        if (!($barsModel = $this->barsModel->load($id))) {
            $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try {
            $barsModel->delete();
            $this->messageManager->addSuccess(__('This notification bar has been deleted!'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete this notification bar!'));
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}
