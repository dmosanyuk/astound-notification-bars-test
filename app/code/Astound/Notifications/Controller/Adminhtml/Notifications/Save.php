<?php
/**
 * Astound Notifications Adminhtml Save Bar action.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Controller\Adminhtml\Notifications;

use Astound\Notifications\Controller\Adminhtml\Notifications;
use Magento\Backend\App\Action;
use Astound\Notifications\Model\BarsFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 * @package Astound\Notifications\Controller\Adminhtml\Notifications
 */
class Save extends Notifications
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Astound_Notifications::notification_bars';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var BarsFactory
     */
    protected $model;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     * @param BarsFactory $model
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        BarsFactory $model,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->model = $model;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            $barModelFactory = $this->model->create();
            $id = $this->getRequest()->getParam('entity_id');

            if ($id) {
                $barModelFactory->load($id);
            }

            $barModelFactory->setTitle(
                $this->getRequest()->getPost('title')
            )->setStoreIds(
                implode(',', $this->getRequest()->getPost('store_ids'))
            )->setContent(
                $this->getRequest()->getPost('content')
            )->setBackgroundColor(
                $this->getRequest()->getPost('background_color')
            )->setStatus(
                $this->getRequest()->getPost('status')
            )->setPriority(
                $this->getRequest()->getPost('priority')
            );

            $this->_eventManager->dispatch(
                'notification_bar_save',
                ['notification_bar' => $barModelFactory, 'request' => $this->getRequest()]
            );

            if (!$this->dataProcessor->validate($data)) {
                return $resultRedirect->setPath('*/*/edit', ['id' => $barModelFactory->getId(), '_current' => true]);
            }

            try {
                $barModelFactory->save();
                $this->messageManager->addSuccess(__('Notification bar has been saved'));
                $this->dataPersistor->clear('notification_bar');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['entity_id' => $barModelFactory->getId(),
                            '_current' => true]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Post.'));
            }

            $this->dataPersistor->set('notification_bar', $data);

            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
