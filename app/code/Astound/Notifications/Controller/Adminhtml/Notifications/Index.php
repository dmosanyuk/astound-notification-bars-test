<?php
/**
 * Astound Notifications bar edit form Index action.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Controller\Adminhtml\Notifications;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Astound\Notifications\Controller\Adminhtml\Notifications
 */
class Index extends Action
{
    /**
     * @var bool|PageFactory
     */
    protected $_resultPageFactory = false;

    /**
     * @var $_resultPage
     */
    protected $_resultPage;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Astound_Notifications::marketing_notifications');
    }

    /**
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Astound_Notifications::marketing_notifications');
        $resultPage->getConfig()->getTitle()->prepend((__('Notification Bars List Management')));
        $resultPage->addBreadcrumb(__('Notification Bars'), __('Notification Bars'));

        return $this;
    }
}
