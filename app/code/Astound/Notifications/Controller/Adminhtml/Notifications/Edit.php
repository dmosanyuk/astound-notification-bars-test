<?php
/**
 * Astound Notifications bar edit form Edit action.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Controller\Adminhtml\Notifications;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;
use Astound\Notifications\Model\BarsFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

/**
 * Class Edit
 * @package Astound\Notifications\Controller\Adminhtml\Notifications
 */
class Edit extends Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Astound_Notifications::notification_bars';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Bars Factory Model
     *
     * @var BarsFactory
     */
    protected $modelBarsFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        BarsFactory $modelBarsFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->modelBarsFactory = $modelBarsFactory;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Astound_Notifications::notification_bars')
            ->addBreadcrumb(__('Notification Bar'), __('Astound Notification Bar'))
            ->addBreadcrumb(__('Manage Notification Bars'), __('Manage Notification Bars'));
        return $resultPage;
    }

    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->modelBarsFactory->create();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This notification bar no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('notification_bar', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Notification Bar') : __('New Notification Bar'),
            $id ? __('Edit Notification Bar') : __('New Notification Bar')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Notification Bars'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Notification Bar'));

        return $resultPage;
    }
}
