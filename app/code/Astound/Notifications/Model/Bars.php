<?php
/**
 * Astound Notifications Bars model.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Bars
 * @package Astound\Notifications\Model
 */
class Bars extends AbstractModel
{
    /**
     * Bars cache tag
     */
    const CACHE_TAG = 'notification_bars';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'notification_bar';

    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Astound\Notifications\Model\ResourceModel\Bars');
    }
}
