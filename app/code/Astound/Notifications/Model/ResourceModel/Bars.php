<?php
/**
 * Astound Notifications Bars ResourceModel.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Bars
 * @package Astound\Notifications\Model\ResourceModel
 */
class Bars extends AbstractDb
{
    /**
     * Bars constructor.
     *
     * @param Context $context
     * @param null $resourcePrefix
     */
    public function __construct(
        Context $context,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('notification_bars_table', 'entity_id');
    }
}
