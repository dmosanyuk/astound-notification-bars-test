<?php
/**
 * Astound Notifications Bars ResourceModel Collection.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Model\ResourceModel\Bars;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Astound\Notifications\Model\ResourceModel\Bars
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Astound\Notifications\Model\Bars', 'Astound\Notifications\Model\ResourceModel\Bars');
    }
}
