<?php
/**
 * Astound Notifications Ui Component Filter Interface.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

declare(strict_types=1);

namespace Astound\Notifications\Ui\Component;

use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;

/**
 * Interface AddFilterInterface
 * @package Astound\Notifications\Ui\Component
 */
interface AddFilterInterface
{
    /**
     * Adds custom filter to search criteria builder based on received filter.
     *
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Filter $filter
     * @return void
     */
    public function addFilter(SearchCriteriaBuilder $searchCriteriaBuilder, Filter $filter);
}
