<?php
/**
 * Astound Notifications Grid Renderer UrlBuilder.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Block\Adminhtml\Bar\Grid\Renderer\Action;

use Magento\Framework\UrlInterface;

/**
 * Class UrlBuilder
 * @package Astound\Notifications\Block\Adminhtml\Bar\Grid\Renderer\Action
 */
class UrlBuilder
{
    /**
     * @var UrlInterface
     */
    protected $frontendUrlBuilder;

    /**
     * UrlBuilder constructor.
     * @param UrlInterface $frontendUrlBuilder
     */
    public function __construct(UrlInterface $frontendUrlBuilder)
    {
        $this->frontendUrlBuilder = $frontendUrlBuilder;
    }

    /**
     * Get action url
     *
     * @param string $routePath
     * @param string $scope
     * @param string $store
     * @return string
     */
    public function getUrl($routePath, $scope, $store)
    {
        if ($scope) {
            $this->frontendUrlBuilder->setScope($scope);
            $href = $this->frontendUrlBuilder->getUrl(
                $routePath,
                [
                    '_current' => false,
                    '_nosid' => true,
                    '_query' => [\Magento\Store\Model\StoreManagerInterface::PARAM_NAME => $store]
                ]
            );
        } else {
            $href = $this->frontendUrlBuilder->getUrl(
                $routePath,
                [
                    '_current' => false,
                    '_nosid' => true
                ]
            );
        }

        return $href;
    }
}
