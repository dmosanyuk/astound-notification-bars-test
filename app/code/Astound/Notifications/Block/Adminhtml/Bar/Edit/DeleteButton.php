<?php
/**
 * Astound Notifications bar edit form DeleteButton.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Block\Adminhtml\Bar\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\Http;

/**
 * Class DeleteButton
 * @package Astound\Notifications\Block\Adminhtml\Bar\Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @var Http
     */
    protected $httpRequest;

    /**
     * DeleteButton constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param Http $httpRequest
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Http $httpRequest
    ) {
        parent::__construct($context, $registry);
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $params = $this->httpRequest->getParams();
        if (isset($params['entity_id'])) {
            $data = [
                'label' => __('Delete'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\''
                    . __('Are you sure you want to delete this notification bar?')
                    . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 30,
            ];
        }
        return $data;
    }

    /**
     * Get URL for delete button
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        $data = $this->httpRequest->getParams();
        return $this->getUrl('*/*/delete', ['entity_id' => $data['entity_id']]);
    }
}
