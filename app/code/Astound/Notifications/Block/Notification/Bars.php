<?php
/**
 * Astound Notifications frontend rendering block Bars.
 *
 * @category  Astound
 * @package   Astound\Notifications
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Astound\Notifications\Block\Notification;

use Magento\Framework\View\Element\Template;
use Astound\Notifications\Model\BarsFactory;

/**
 * Class Bars
 * @package Astound\Notifications\Block\Notification
 */
class Bars extends Template
{
    /**
     * Bars Factory Model
     *
     * @var BarsFactory
     */
    protected $modelBarsFactory;

    /**
     * Bars constructor.
     *
     * @param BarsFactory $modelBarsFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        BarsFactory $modelBarsFactory,
        Template\Context $context,
        array $data = []
    ) {
        $this->modelBarsFactory = $modelBarsFactory;
        parent::__construct($context, $data);
    }

    /**
     * Filtered collection with notification bars
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getBars()
    {
        $barsFactory = $this->modelBarsFactory->create()
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->setOrder('priority','ASC');

        return $barsFactory;
    }

    /**
     * Checking if notification bar has permissions for current store
     *
     * @param $storeIdsString
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBarByStores($storeIdsString)
    {
        $barStoreIds = explode(',', $storeIdsString);
        $currentStoreId = $this->_storeManager->getStore()->getId();
        if ((in_array($currentStoreId, $barStoreIds)) || (in_array(0, $barStoreIds))) {

            return true;
        }

        return false;
    }
}
